/**
 * 
 */
package com.hcl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.hcl.exception.UserException;
import com.hcl.model.User;

/**
 * @author nithish.kumarb
 *
 */
public class UserDaoImpl implements UserDao {
	Connection con = MysqlConnectionCheck.GetConnection();

	@Override
	public User createTable(User user) throws UserException {
		try {
			Statement createCommand = con.createStatement();
			String sql = "CREATE TABLE User " + "(id INTEGER not NULL, " + " name VARCHAR(255), " + " password BIGINT, "
					+ " PRIMARY KEY ( id ))";
			createCommand.executeUpdate(sql);
			System.out.println("table created in database");
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String readTable(int id) throws UserException {
		System.out.println("reading name from our database");
		String query = "SELECT * FROM USER WHERE id= ?";
		PreparedStatement insertCommand;
		try {
			insertCommand = con.prepareStatement(query);

			insertCommand.setInt(1, id);
			ResultSet row = insertCommand.executeQuery();
			while (row.next()) {
				System.out.println("welcome " + row.getString("name") + " !");
			}
			con.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public User updateTable(User user) throws UserException {
		System.out.println("updating values into database");
		String query = "UPDATE USER SET name=? where id=?";
		try {
			PreparedStatement updateQuery = con.prepareStatement(query);
			updateQuery.setString(1, user.getName());
			updateQuery.setInt(2, user.getId());
			updateQuery.executeUpdate();
			System.out.println("name successfully updated for the id:" + user.getId());
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public User insertIntoTable(User user) throws UserException {
		try {
			System.out.println("values inserting into database");
			String query = "INSERT INTO USER(id,name,password)" + "values(?,?,?)";

			PreparedStatement insertCommand = con.prepareStatement(query);
			insertCommand.setInt(1, user.getId());
			insertCommand.setString(2, user.getName());
			insertCommand.setLong(3, user.getPassword());
			int row = insertCommand.executeUpdate();
			System.out.println("the given values has been successfully inserted " + row);
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
