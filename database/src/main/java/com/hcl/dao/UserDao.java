/**
 * 
 */
package com.hcl.dao;

import com.hcl.exception.UserException;
import com.hcl.model.User;

/**
 * @author nithish.kumarb
 *
 */
public interface UserDao {
	public abstract User createTable(User user) throws UserException;

	public abstract User insertIntoTable(User user) throws UserException;

	public abstract String readTable(int id) throws UserException;

	public abstract User updateTable(User user) throws UserException;

}
