/**
 * 
 */
package com.hcl.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author nithish.kumarb
 *
 */
public class MysqlConnectionCheck {
	public static Connection GetConnection() {
		Connection connection1 = null;

		try {
			String driverName = "com.mysql.cj.jdbc.Driver";
			String url = "jdbc:mysql://localhost:3306/jdbc";
			Class.forName(driverName);
			 connection1 = DriverManager.getConnection(url, "root", "root");
			 System.out.println(connection1 != null ? "connection established" : "connection failed");
		}

		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return connection1;
	}
}
