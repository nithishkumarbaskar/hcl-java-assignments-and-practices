/**
 * 
 */
package com.hcl.model;

import java.util.Scanner;

import com.hcl.exception.UserException;
import com.hcl.service.UserInterface;
import com.hcl.service.UserInterfaceImpl;

/**
 * @author nithish.kumarb
 *
 */
public class InputArgs {
	int id;
	String name;
	long password;
	public User userInput() throws UserException {
		Scanner scanner = new Scanner(System.in);
		System.out.println("please enter valid user id");
		 id = scanner.nextInt();
		System.out.println("please enter valid user name");
		name = scanner.next();
		System.out.println("please enter valid password");
		password = scanner.nextLong();
		User user = new User(id, name, password);
		UserInterface userInterface = new UserInterfaceImpl();
		userInterface.validmethod(user);
		return null;
	}
	public InputArgs() {
		super();
	}
}
