/**
 * 
 */
package com.hcl.service;

import com.hcl.dao.UserDao;
import com.hcl.dao.UserDaoImpl;
import com.hcl.exception.UserException;
import com.hcl.model.User;

/**
 * @author nithish.kumarb
 *
 */
public class UserInterfaceImpl implements UserInterface {
	UserDao userDao = new UserDaoImpl();

	@Override
	public User validmethod(User user) throws UserException {
		if (user.getId() > 5) {
			System.out.println("valid user");
		} else {
			System.out.println("Sorry invalid ID! ");
		}
		return user;

	}

	@Override
	public User createmethod(User user) throws UserException {
		userDao.createTable(user);
		return user;
	}

	@Override
	public User insertmethod(User user) throws UserException {
		userDao.insertIntoTable(user);
		return null;
	}

	@Override
	public User readmethod(User user) throws UserException {
		userDao.readTable(user.getId());
		return null;
	}

	@Override
	public User updatemethod(User user) throws UserException {
		userDao.updateTable(user);
		return null;
	}

}
