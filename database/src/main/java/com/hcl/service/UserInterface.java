/**
 * 
 */
package com.hcl.service;

import com.hcl.exception.UserException;
import com.hcl.model.User;

/**
 * @author nithish.kumarb
 *
 */
public interface UserInterface {
public abstract User validmethod (User user) throws UserException;
public abstract User createmethod(User user) throws UserException;
public abstract User insertmethod(User user) throws UserException;
public abstract User readmethod(User user) throws UserException;
public abstract User updatemethod(User user) throws UserException;
}
