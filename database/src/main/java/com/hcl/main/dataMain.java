/**
 * 
 */
package com.hcl.main;

import java.util.Scanner;

import com.hcl.exception.UserException;
import com.hcl.model.InputArgs;
import com.hcl.model.User;
import com.hcl.service.UserInterface;
import com.hcl.service.UserInterfaceImpl;

/**
 * 45678
 * 
 * @author nithish.kumarb
 *
 */
public class dataMain {

	/**
	 * @param args
	 * @throws UserException
	 */
	public static void main(String[] args) throws UserException {
		Scanner scanner = new Scanner(System.in);
		System.out.println("please enter valid user id");
		int id = scanner.nextInt();
		System.out.println("please enter valid user name");
		String name = scanner.next();
		System.out.println("please enter valid password");
		long password = scanner.nextLong();
		User user = new User(id, name, password);
		UserInterface userInterface = new UserInterfaceImpl();
		userInterface.validmethod(user);
		System.out.println("what function do you want to do?");
		System.out.println("a. create table");
		System.out.println("b. insert into table");
		System.out.println("c. read data from table");
		System.out.println("d. update data in table");
		System.out.println("please respond with valid option");
		String answer = scanner.next();
		if (answer.equals("a")) {
			userInterface.createmethod(user);
		} else if (answer.equals("b")) {
			userInterface.insertmethod(user);
		} else if (answer.equals("c")) {
			userInterface.readmethod(user);
		} else if (answer.equals("d")) {
			userInterface.updatemethod(user);
		} else {
			System.out.println("you have not entered the correct option!");
		}
	}

}
