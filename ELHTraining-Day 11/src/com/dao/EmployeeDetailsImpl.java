/**
 * 
 */
package com.dao;

import java.util.ArrayList;
import java.util.List;

import com.exception.EmployeeException;
import com.model.Employee;

/**
 * @author nithish.kumarb
 *
 */
public class EmployeeDetailsImpl implements EmployeeDetails {
	public List<Employee> init() {
		Employee employee1 = new Employee(51998860, "Nithish", 8680824243l);
		Employee employee2 = new Employee(23456789, "Kumar", 6383218134l);
		Employee employee3 = new Employee(19876543, "Raj", 9688797003l);
		Employee employee4 = new Employee(55555555, "Vijay", 6786543746l);
		List<Employee> Department = new ArrayList<Employee>();
		Department.add(employee1);
		Department.add(employee2);
		Department.add(employee3);
		Department.add(employee4);
		return Department;
	}

	@Override
	public Employee getDetails(int employeeId) throws EmployeeException {
		Employee employeeDetails = null;
		List<Employee> Department = this.init();
		for (Employee employee : Department) {
			if (employee.getEmployeeId() == employeeId) {
				employeeDetails = employee;
				System.out.println("employee is present in this organization");
				break;
			} else {
				System.out.println("No such employee");
				break;
			}
		}
		return employeeDetails;
	}

}
