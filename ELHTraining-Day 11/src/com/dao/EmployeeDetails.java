/**
 * 
 */
package com.dao;

import com.exception.EmployeeException;
import com.model.Employee;

/**
 * @author nithish.kumarb
 *
 */
public interface EmployeeDetails {
	public abstract Employee getDetails(int employeeId) throws EmployeeException;
}
