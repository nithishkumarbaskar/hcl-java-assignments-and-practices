/**
 * 
 */
package com.main;

import java.util.Scanner;

import com.exception.EmployeeException;
import com.model.Employee;
import com.service.EmployeeService;
import com.service.EmployeeServiceImpl;

/**
 * @author nithish.kumarb
 *
 */
public class LoginMain {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the Employee id:");
		EmployeeService employeeService = new EmployeeServiceImpl();
		int employeeId = input.nextInt();
		try {
			Employee employee = employeeService.check(employeeId);
			if (employee != null) {
				System.out.println("Welcome to this Practice App " + employee.getEmployeeName());
				System.out.println("The employee details are: \n" + "Employee id is " + employee.getEmployeeId() + ".\n"
						+ "Employee Full name is " + employee.getEmployeeName() + ".\n" + "Employee mobile number is "
						+ employee.getMobileNumber() + ".\n");
			}
		} catch (EmployeeException e) {
			System.err.println(e);
		}
	}

}
