/**
 * 
 */
package com.exception;

/**
 * @author nithish.kumarb
 *
 */
public class EmployeeException extends Exception {
	private String error;

	public EmployeeException(String error) {
		super();
		this.error = error;
	}

	@Override
	public String getMessage() {
		return this.error;
	}

}
