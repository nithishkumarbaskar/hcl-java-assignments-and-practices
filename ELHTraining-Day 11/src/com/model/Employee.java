/**
 * 
 */
package com.model;

// this file is used to create a model of employee details that needs to be followed in other files
/**
 * @author nithish.kumarb
 *
 */
public class Employee {
	private int employeeId;
	private String employeeName;
	private long mobileNumber;

	public Employee() {
		super();
	}

	public Employee(int employeeId, String employeeName, long mobileNumber) {
		super();
		this.employeeId = employeeId;
		this.employeeName = employeeName;
		this.mobileNumber = mobileNumber;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public long getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

}
