/**
 * 
 */
package com.service;

import com.exception.EmployeeException;
import com.model.Employee;

/**
 * @author nithish.kumarb
 *
 */
public interface EmployeeService {
	public abstract Employee check(int employeeId) throws EmployeeException;

}
