/**
 * 
 */
package com.service;

import com.dao.EmployeeDetails;
import com.dao.EmployeeDetailsImpl;
import com.exception.EmployeeException;
import com.model.Employee;

/**
 * @author nithish.kumarb
 *
 */
public class EmployeeServiceImpl implements EmployeeService {
	Employee employee = null;

	@Override
	public Employee check(int employeeId) throws EmployeeException {
		int lengthOfId = String.valueOf(employeeId).length();
		if ((lengthOfId >= 8)) {
			EmployeeDetails employeeDetails = new EmployeeDetailsImpl();
			employee = employeeDetails.getDetails(employeeId);
		} else {
			throw new EmployeeException("There is no employee with the id " + employeeId + " in this organization");
		}
		return employee;
	}

}
