package com;
import java.util.Scanner;
public class DetailsMain {

	public static void main(String[] args) {
		DetailsTemplate empDetail1=new DetailsTemplate();
		empDetail1.empNo=5;
		empDetail1.age=22;
		empDetail1.name="ajith";
		DetailsTemplate empDetail2=new DetailsTemplate();
		empDetail2.empNo=8;
		empDetail2.age=29;
		empDetail2.name="kumar";
		@SuppressWarnings("resource")
		Scanner employeeNumber=new Scanner(System.in);
	    System.out.println("Enter employee employee number: ");
	    int empNum= employeeNumber.nextInt();
		if(empNum==5) {
			String allDetails=empDetail1.display();
			System.out.println(allDetails);
		}
		else if(empNum==8) {
			String allDetails=empDetail2.display();
			System.out.println(allDetails);
		}
		else {
			System.out.println("there is no employee with the employee number "+empNum+" in this organization");
		}
		

	}

}
