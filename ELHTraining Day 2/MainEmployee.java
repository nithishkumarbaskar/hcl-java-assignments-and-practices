package com;

public class MainEmployee {

	public static void main(String[] args) {
		Employee employee1 = new Employee();
		employee1.empNumber = 1998;
		employee1.name = "nithish";
		employee1.salary = 40000.08f;
		Employee employee2 = new Employee();
		employee2.empNumber = 1999;
		employee2.name = "kumar";
		employee2.salary = 80000.08f;
		System.out.println("the employee number: " + employee1.empNumber);
		System.out.println("the employee name: " + employee1.name);
		System.out.println("the employee salary: " + employee1.salary);
		System.out.println("the employee number: " + employee2.empNumber);
		System.out.println("the employee name: " + employee2.name);
		System.out.println("the employee salary: " + employee2.salary);

	}

}
