package com;

public class ArraySumMain {

	public static void main(String[] args) {
		int[] numbers = { 0, 3, 5, 8, 9 };
		ArraySum arraySum = new ArraySum();
		int total = arraySum.sumArray(numbers);
		System.out.println(total);
	}

}
