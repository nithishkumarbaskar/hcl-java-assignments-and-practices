package com.hcl.userservice.service;
import java.util.List;
import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.hcl.userservice.model.Pet;
@FeignClient(name="pet-service/Pet",url="pet-service/Pet")
public interface PetFeignClient {
	@GetMapping(value="/pets")
	public abstract ResponseEntity<List<Pet>> getPets();
	
	@GetMapping(value="/pets/petDetail/{petId}")
	public abstract Optional<Pet> getPetById(@PathVariable("petId") long petId);
	

	@GetMapping(value="/myPets/{userId}")
	public abstract ResponseEntity<List<Pet>> getPetsByUserId(@PathVariable("userId") long userId);
}
