package com.hcl.userservice.service;
import java.util.List;

import com.hcl.userservice.exception.UserException;
import com.hcl.userservice.model.Pet;
import com.hcl.userservice.model.User;
public interface UserService {
	public abstract String addUser(User user) throws UserException;

	public abstract User updateUser(User user) throws UserException;

	public abstract List<User> listUsers() throws UserException;

	public abstract User getUserById(long userId)throws UserException;

	public abstract String removeUser(long userId) throws UserException;

	public abstract List<Pet> authenticateUser(String userName, String password) throws UserException;

	public abstract User findByUserName(String username) throws UserException;

	public abstract int buyPet(long userId, long petId) throws UserException;

	public abstract String deleteUser(long userId) throws UserException;

	public abstract List<Pet> getMyPets(long userId) throws UserException;
	
	public abstract String loginApp(String userName, String password) throws UserException;

}
