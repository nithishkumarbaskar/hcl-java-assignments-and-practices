package com.hcl.userservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.userservice.exception.UserException;
import com.hcl.userservice.model.Pet;
import com.hcl.userservice.model.User;
import com.hcl.userservice.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PetFeignClient petFeignClient;

	@Override
	public String addUser(User user) throws UserException {
		String addStatus = null;
		User userOne = userRepository.findByName(user.getUserName());

		if (userOne == null) {

			User userTwo = userRepository.save(user);
			if (userTwo != null) {

				addStatus = "registered successfully";
				return addStatus;

			} else {

				throw new UserException("Sorry you can't register  at the moment, please try again later");
			}

		} else {
			addStatus = "you have been registered with respect to our database";
			return addStatus;
		}
	}

	@Override
	public User updateUser(User user) throws UserException {
		User userOne = null;
		Optional<User> optionalUser = userRepository.findById(user.getId());
		if (optionalUser.isPresent()) {
			userOne = userRepository.save(user);
			return userOne;
		} else {
			throw new UserException("Sorry we can't update the user details");
		}

	}

	@Override
	public List<User> listUsers() throws UserException {
		List<User> users = null;
		users = userRepository.findAll();
		if (users.size() > 0) {
			return users;
		} else {
			throw new UserException(
					"Currently there is no users in the database,if you wanna be a user please register");
		}
	}

	@Override
	public User getUserById(long userId) throws UserException {
		User user = null;
		Optional<User> optionalUser = userRepository.findById(userId);

		if (optionalUser.isPresent()) {
			user = optionalUser.get();
			return user;
		} else {
			throw new UserException("invalid User id,kindly check from your end");
		}

	}

	@Override
	public String removeUser(long userId) throws UserException {
		String removeStatus = null;
		Optional<User> optionalUser = userRepository.findById(userId);

		if (optionalUser.isPresent()) {
			userRepository.deleteById(userId);
			removeStatus = "User  Is Removed";
			return removeStatus;
		} else {
			throw new UserException("we can't delete that user id");

		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pet> authenticateUser(String userName, String password) throws UserException {
		List<Pet> pets = null;

		User user = userRepository.findByNameAndPassword(userName, password);

		if (user != null) {

			pets = (List<Pet>) petFeignClient.getPets();
			return pets;
		} else {
			throw new UserException("invalid credentials");
		}

	}

	@Override
	public User findByUserName(String username) throws UserException {
		User user = userRepository.findByName(username);
		if (user != null) {
			return user;
		} else {
			throw new UserException("we can't find by user name");
		}
	}

	@Override
	public int buyPet(long userId, long petId) throws UserException {
		int numberOfRowAffected = 0;
		Optional<User> optionalUser = userRepository.findById(userId);
		Optional<Pet> optionalPet = petFeignClient.getPetById(petId);
		if (optionalUser.isPresent() && optionalPet.isPresent()) {

			numberOfRowAffected = userRepository.updatePetByUserId(userId, petId);
			return numberOfRowAffected;
		} else {
			throw new UserException("sorry you can't buy the pet");
		}
	}

	@Override
	public String deleteUser(long userId) throws UserException {
		String removeStatus = null;
		Optional<User> optionalUser = userRepository.findById(userId);

		if (optionalUser.isPresent()) {
			userRepository.deleteById(userId);
			removeStatus = "User Record Is Removed";
			return removeStatus;

		} else {
			throw new UserException("User Record Is Not Present Is DB");

		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pet> getMyPets(long userId) {
		return (List<Pet>) petFeignClient.getPetsByUserId(userId);
	}

	@Override
	public String loginApp(String userName, String password) throws UserException {
		String loginStatus = null;

		User user = userRepository.findByNameAndPassword(userName, password);

		if (user != null) {
			loginStatus = "Login Successful";
			return loginStatus;
		} else {
			throw new UserException("sorry login failure");
		}

	}

}
