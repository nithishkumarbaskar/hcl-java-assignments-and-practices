package com.hcl.userservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.userservice.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
	@Query("select u from User u where u.userName = :userName")
	public abstract User findByName(@Param("userName") String userName);

	@Query("select u from User u where u.userName = :userName and u.userPassword = :userPassword")
	public abstract User findByNameAndPassword(@Param("userName") String userName,
			@Param("userPassword") String userPassword);

	@Transactional
	@Modifying
	@Query(value = "UPDATE pets SET PET_OWNERID= :userId WHERE ID = :petId AND PET_OWNERID IS NULL", nativeQuery = true)
	public abstract int updatePetByUserId(@Param("userId") long userId, @Param("petId") long petId);

	public abstract List<User> findAll();
}
