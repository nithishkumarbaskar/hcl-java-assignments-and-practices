/**
 * 
 */
package com.hcl.userservice.exception;

/**
 * @author nithish.kumarb
 *
 */
public class UserException extends Exception {
	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public UserException(String errorMessage) {
		super();
		this.errorMessage = errorMessage;
	}

	@Override
	public String getMessage() {
		return this.errorMessage;
	}
}
