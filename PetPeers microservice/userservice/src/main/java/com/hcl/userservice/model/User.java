package com.hcl.userservice.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.lang.NonNull;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.hcl.userservice.model.Pet;
@Entity
@Table(name = "pet_user")
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	@Column(name = "user_name")
	@NonNull
	private String userName;

	@Column(name = "user_password")
	@NonNull
	private String userPassword;



/*@OneToMany(mappedBy = "owner", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH,
			CascadeType.REFRESH }, fetch = FetchType.LAZY)
	@JsonManagedReference
	private Set<Pet> pets;*/

	public User(long id, String userName, String userPassword, Set<Pet> pets) {
		super();
		this.id = id;
		this.userName = userName;
		this.userPassword = userPassword;
	}

	public User() {
		super();

	}

	public User(String userName, String userPassword, Set<Pet> pets) {
		super();
		this.userName = userName;
		this.userPassword = userPassword;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}




}
