package com.hcl.petservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.petservice.exception.PetException;
import com.hcl.petservice.model.Pet;
import com.hcl.petservice.repository.PetRepository;

@Service
public class PetServiceImpl implements PetService {
	@Autowired
	private PetRepository petRepository;

	@Override
	public String savePet(Pet pet) throws PetException {
		String saveStatus = null;
		if (pet != null && pet.getName() != null && pet.getPlace() != null && pet.getAge() > 0) {

			Pet saveObj = petRepository.save(pet);
			if (saveObj != null) {
				saveStatus = "pet successfully added";
			} else {
				throw new PetException("pet not successfully added");
			}
		}

		return saveStatus;
	}

	@Override
	public List<Pet> getAllPets() throws PetException {
		List<Pet> allPets = null;
		allPets = petRepository.findAllRecord();
		if (allPets != null) {
			return allPets;
		} else {
			throw new PetException("there are no pets available in database");
		}

	}

	@Override
	public Pet getPetById(long petId) throws PetException {
		Pet pet = null;
		Optional<Pet> optionalPet = petRepository.findById(petId);
		if (optionalPet.isPresent()) {
			pet = optionalPet.get();
			return pet;
		} else {
			throw new PetException("there is no pet with the given id");
		}

	}

	@Override
	public Pet updatePet(Pet pet) throws PetException {
		Pet petTwo = null;
		Optional<Pet> optionalPet = petRepository.findById(pet.getId());
		if (optionalPet.isPresent()) {
			petTwo = petRepository.save(pet);
			return petTwo;
		} else {
			throw new PetException("Sorry sir, we can't update your pet");
		}

	}

	@Override
	public String deletePetById(long petId) throws PetException {
		String removalStatus = null;
		Optional<Pet> optionalPet = petRepository.findById(petId);

		if (optionalPet.isPresent()) {
			petRepository.deleteById(petId);
			removalStatus = "Pet Record Is Removed";
			return removalStatus;
		} else {
			throw new PetException("Sorry we can't delete the pet");

		}

	}

}
