package com.hcl.petservice.service;

import java.util.List;

import com.hcl.petservice.exception.PetException;
import com.hcl.petservice.model.Pet;

public interface PetService {
	public abstract String savePet(Pet pet) throws PetException;

	public abstract List<Pet> getAllPets() throws PetException;

	public abstract Pet getPetById(long petId) throws PetException;

	public abstract Pet updatePet(Pet pet) throws PetException;

	public abstract String deletePetById(long petId) throws PetException;
}
