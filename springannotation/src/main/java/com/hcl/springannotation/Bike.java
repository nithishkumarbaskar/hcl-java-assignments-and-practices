/**
 * 
 */
package com.hcl.springannotation;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author nithish.kumarb
 *
 */
public class Bike {
	@Autowired
	private Mileage mileage;

	public void BikeType() {
		System.out.println("KTM is only available");
		mileage.mileageDetails();

	}
}
