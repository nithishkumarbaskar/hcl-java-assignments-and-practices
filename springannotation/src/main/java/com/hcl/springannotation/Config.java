package com.hcl.springannotation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {
	@Bean
	public Bike type() {
		return new Bike();
	}

	@Bean
	public Mileage deta() {
		return new Mileage();
	}
}
