/**
 * 
 */
package com.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.model.Employee;

/**
 * @author NITHISH KUMAR
 *
 */
public class EmployeeServiceImpl implements EmployeeService {

	@Override
	public List employeeSearchBySalary( List employeeList, float salary) {
		List employees = new ArrayList();
for (Iterator iterator = employeeList.iterator(); iterator.hasNext();) {
	Employee object = (Employee) iterator.next();
	if(object.getSalary()>salary) {
		employees.add(object);
	}
}
		return employees;
	}

}
