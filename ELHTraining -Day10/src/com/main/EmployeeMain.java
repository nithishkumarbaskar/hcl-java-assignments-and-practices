package com.main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.model.Employee;
import com.service.EmployeeService;
import com.service.EmployeeServiceImpl;

public class EmployeeMain {

	public static void main(String[] args) {
		Employee employee1 = new Employee(23, "Nithish", 55000.09f);
		Employee employee2 = new Employee(24, "Kumar", 9000.09f);
		Employee employee3 = new Employee(25, "Raj", 6000.09f);
		List list = new ArrayList();
		list.add(employee1);
		list.add(employee2);
		list.add(employee3);

		EmployeeService employeeService = new EmployeeServiceImpl();

		List details = employeeService.employeeSearchBySalary(list, 5000.00f);
		for (Iterator iterator = details.iterator(); iterator.hasNext();) {
			Object object = (Object) iterator.next();
			if (object instanceof Employee) {
				Employee new_name = (Employee) object;
				System.out.println(new_name.getEmployeeName());
			}
		}
	}
}
