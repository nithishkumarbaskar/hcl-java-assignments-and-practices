package com.main;

import com.model.Employee;
import com.service.EmployeeService;

public class MainEmployee {

	public static void main(String[] args) {
		EmployeeService employeeService = new EmployeeService();
		Employee [] all=employeeService.getAllEmployees();
		for (Employee employee : all) {
			System.out.println(employee.getEmployeeId());
			System.out.println(employee.getEmployeeName());
			System.out.println(employee.getEmployeeSalary());
		}
	}

}
