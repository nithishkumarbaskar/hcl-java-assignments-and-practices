/**
 * 
 */
package com.service;

import com.model.Employee;

/**
 * @author NITHISH KUMAR
 *
 */
public class EmployeeService {
	public Employee getDetails() {
		Employee employee = new Employee();
		employee.setEmployeeId(1);
		employee.setEmployeeName("nithish");
		employee.setEmployeeSalary(44000.09f);
		return employee;
	}

	public Employee getDetails(int id) {
		Employee employee = null;
		if (id == 1) {
			employee=new Employee();
			employee.setEmployeeId(1);
			employee.setEmployeeName("nithish");
			employee.setEmployeeSalary(44000.09f);
			return employee;
		} 
		else if (id == 2) {
			employee=new Employee();
			employee.setEmployeeId(2);
			employee.setEmployeeName("kumar");
			employee.setEmployeeSalary(44600.09f);
			return employee;
		} 
		else {
			return (employee);
		}
	}
	public Employee [] getAllEmployees() {
		Employee employee1=new Employee();
		employee1.setEmployeeId(1);
		employee1.setEmployeeName("nithish");
		employee1.setEmployeeSalary(44000.09f);
		Employee employee2=new Employee();
		employee2.setEmployeeId(2);
		employee2.setEmployeeName("kumar");
		employee2.setEmployeeSalary(41000.09f);
		Employee [] allEmployee=new Employee[2];
		allEmployee[0]=employee1;
		allEmployee[1]= employee2;
		return allEmployee;
		
	}
}
