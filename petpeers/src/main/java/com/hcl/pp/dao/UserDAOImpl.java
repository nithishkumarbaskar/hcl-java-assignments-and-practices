package com.hcl.pp.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.repository.PetRepository;
import com.hcl.pp.repository.UserRepository;

@Repository
public class UserDAOImpl implements UserDAO {


	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PetRepository petRepository;

	@Override
	public String addUser(User user) {
		String userAddStatus = null;

		User userOne = userRepository.findByName(user.getUserName());

		if (userOne == null) {

			User userTwo = userRepository.save(user);
			if (userTwo != null) {

				userAddStatus = "registered successfully";

			} else {

				userAddStatus = "Not registered please try with valid values";

			}

		} else {
			userAddStatus = "you have been registered with respect to our database";
		}


		return userAddStatus;
	}

	@Override
	public User updateUser(User user) {
		User userOne = null;
		Optional<User> optionalUser = userRepository.findById(user.getId());
		if (optionalUser.isPresent()) {
			userOne = userRepository.save(user);
		}


		return userOne;
	}

	@Override
	public List<User> listUsers() {
		List<User> users = null;
		users = userRepository.findAll();
		if (users.size() > 0) {
			return users;
		} else {
			return null;
		}

	}

	@Override
	public User getUserById(long userId) {
		User user = null;
		Optional<User> optionalUser = userRepository.findById(userId);

		if (optionalUser.isPresent()) {
			user = optionalUser.get();
		}
		return user;
	}

	@Override
	public String removeUser(long userId) {

		String removeStatus = null;
		Optional<User> optionalUser = userRepository.findById(userId);

		if (optionalUser.isPresent()) {
			userRepository.deleteById(userId);
			removeStatus = "User  Is Removed";
		} else {
			removeStatus = "No user present";

		}

		return removeStatus;
	}

	@Override
	public List<Pet> authenticateUser(String userName, String password) {
		List<Pet> pets = null;

		User user = userRepository.findByNameAndPassword(userName, password);

		if (user != null) {

			pets = petRepository.findAllRecord();

		}


		return pets;
	}

	@Override
	public User findByUserName(String username) {

		User user = userRepository.findByName(username);

		return user;
	}

	@Override
	public int buyPet(long userId, long petId) {
		int numberOfRowAffected = 0;
		Optional<User> optionalUser = userRepository.findById(userId);
		Optional<Pet> optionalPet = petRepository.findById(petId);

		if (optionalUser.isPresent() && optionalPet.isPresent()) {

			numberOfRowAffected = petRepository.updatePetByUserId(userId, petId);

		}


		return numberOfRowAffected;
	}

	@Override
	public String deleteUser(long userId) {
		String removeStatus = null;
		Optional<User> optionalUser = userRepository.findById(userId);

		if (optionalUser.isPresent()) {
			userRepository.deleteById(userId);
			removeStatus = "User Record Is Removed";

		} else {
			removeStatus = "User Record Is Not Present Is DB";

		}

		return removeStatus;
	}

	@Override
	public List<Pet> getMyPets(long userId) {

		return petRepository.findByUserId(userId);
	}

	@Override
	public String loginApp(String userName, String password) {

		String loginStatus = null;

		User user = userRepository.findByNameAndPassword(userName, password);

		if (user != null) {
			loginStatus = "Login Successful";

		}


		return loginStatus;
	}

}
