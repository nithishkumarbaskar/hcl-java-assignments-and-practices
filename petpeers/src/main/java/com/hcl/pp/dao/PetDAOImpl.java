package com.hcl.pp.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hcl.pp.model.Pet;
import com.hcl.pp.repository.PetRepository;

@Repository
public class PetDAOImpl implements PetDAO {


	@Autowired
	private PetRepository petRepository;

	@Override
	public String savePet(Pet pet) {
		String petStatus = null;
		Pet petTwo = petRepository.save(pet);
		if (petTwo != null) {

			petStatus = "pet successfully added";

		} else {

			petStatus = "pet not added";

		}

		return petStatus;
	}

	@Override
	public List<Pet> getAllPets() {

		return petRepository.findAllRecord();
	}

	@Override
	public Pet getPetById(long petId) {
		Pet pet = null;

		Optional<Pet> optionalPet = petRepository.findById(petId);

		if (optionalPet.isPresent()) {
			pet = optionalPet.get();
		}

		return pet;
	}

	@Override
	public Pet updatePet(Pet pet) {
		Pet petTwo = null;
		Optional<Pet> optionalPet = petRepository.findById(pet.getId());
		if (optionalPet.isPresent()) {
			petTwo = petRepository.save(pet);
		}

		return petTwo;
	}

	@Override
	public String deletePetById(long petId) {
		String petStatusRemoveal = null;
		Optional<Pet> optionalPet = petRepository.findById(petId);

		if (optionalPet.isPresent()) {
			petRepository.deleteById(petId);
			petStatusRemoveal = "Pet Record Is Removed";
		} else {
			petStatusRemoveal = "pet is not present";

		}
		return petStatusRemoveal;
	}

}
