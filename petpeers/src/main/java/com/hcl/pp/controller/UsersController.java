package com.hcl.pp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.pp.model.Pet;
import com.hcl.pp.model.ResponseMessage;
import com.hcl.pp.model.User;
import com.hcl.pp.service.UserService;

@RestController
public class UsersController {


	@Autowired
	UserService userService;

	@CrossOrigin(origins = "http://localhost:3000")
	@PostMapping(value = "/user/add")
	public ResponseEntity<ResponseMessage> addUser(@Valid @RequestBody User user) {
		String addStatus = userService.addUser(user);
		ResponseMessage responseMessage = new ResponseMessage();
		responseMessage.setMessage(addStatus);
		ResponseEntity<ResponseMessage> responseEntity = new ResponseEntity<ResponseMessage>(responseMessage,
				HttpStatus.CREATED);
		return responseEntity;

	}

	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping(value = "/user/login/{userName}/{userPassword}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Object>> loginUser(@PathVariable String userName, @PathVariable String userPassword) {
		List<Pet> pets = null;
		List<Object> errorMessageObject = null;
		pets = userService.authenticateUser(userName, userPassword);
		ResponseEntity<List<Object>> responseEntity = null;
		if (pets != null) {
			List<Object> petObjects = new ArrayList<Object>();
			petObjects.addAll(pets);
			responseEntity = new ResponseEntity<List<Object>>(petObjects, HttpStatus.OK);
			return responseEntity;
		} else {
			errorMessageObject = new ArrayList<Object>();
			errorMessageObject.add("invalid username and paassword");
			responseEntity = new ResponseEntity<List<Object>>(errorMessageObject, HttpStatus.BAD_REQUEST);

		}


		return responseEntity;

	}

	@CrossOrigin(origins = "http://localhost:3000")
	@GetMapping(value = "/user/loginapp/{userName}/{userPassword}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseMessage> loginApp(@PathVariable String userName, @PathVariable String userPassword) {
		ResponseMessage responseMessage = new ResponseMessage();
		String loginStatus = null;
		ResponseEntity<ResponseMessage> responseEntity = null;
		loginStatus = userService.loginApp(userName, userPassword);
		if (loginStatus != null) {
			responseMessage.setMessage(loginStatus);
			responseEntity = new ResponseEntity<ResponseMessage>(responseMessage, HttpStatus.OK);
		} else {
			loginStatus = "Login Failed Check Your User Name And Password";
			responseMessage.setMessage(loginStatus);
			responseEntity = new ResponseEntity<ResponseMessage>(responseMessage, HttpStatus.OK);
		}

		return responseEntity;

	}

	@GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Object>> listUsers() {
		List<User> users = null;
		List<Object> errorMessageObject = null;
		users = userService.listUsers();
		ResponseEntity<List<Object>> responseEntity = null;
		if (users != null) {
			List<Object> userObject = new ArrayList<Object>();
			userObject.addAll(users);
			responseEntity = new ResponseEntity<List<Object>>(userObject, HttpStatus.FOUND);
			return responseEntity;
		} else {
			errorMessageObject = new ArrayList<Object>();
			errorMessageObject.add("table is empty");
			responseEntity = new ResponseEntity<List<Object>>(errorMessageObject, HttpStatus.BAD_REQUEST);

		}


		return responseEntity;
	}

	@DeleteMapping(value = "/delete/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseMessage> deleteUser(@PathVariable long userId) {
		String deleteStatus = userService.deleteUser(userId);
		ResponseMessage responseMessage = new ResponseMessage();
		responseMessage.setMessage(deleteStatus);
		ResponseEntity<ResponseMessage> responseEntity = new ResponseEntity<ResponseMessage>(responseMessage,
				HttpStatus.GONE);
		return responseEntity;

	}

	@GetMapping(value = "/pets/myPets/{userId}")
	public List<Object> myPets(@PathVariable long userId) {
		List<Pet> pets = null;
		List<Object> errorMessageObject = null;
		pets = userService.getMyPets(userId);
		if (pets != null && pets.size() > 0) {
			List<Object> petsObjects = new ArrayList<Object>();
			petsObjects.addAll(pets);
			return petsObjects;

		} else {
			errorMessageObject = new ArrayList<Object>();
			errorMessageObject.add("no pets available for you");

		}
		return errorMessageObject;

	}

	@GetMapping(value = "/pets/buyPet/{userId}/{petId}")
	public ResponseEntity<ResponseMessage> buyPet(@PathVariable long userId, @PathVariable long petId) {
		ResponseEntity<ResponseMessage> responseEntity = null;
		int numberOfRowAffected = userService.buyPet(userId, petId);
		ResponseMessage responseMessage = new ResponseMessage();
		if (numberOfRowAffected > 0) {
			responseMessage.setMessage("new pet is added for you user");

			return responseEntity = new ResponseEntity<ResponseMessage>(responseMessage, HttpStatus.FOUND);

		} else {
			responseMessage.setMessage("pet no available");
			responseEntity = new ResponseEntity<ResponseMessage>(responseMessage, HttpStatus.BAD_REQUEST);
		}

		return responseEntity;

	}

	@PutMapping(value = "/upadteUser", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> updateUser(@Valid @RequestBody User user) {

		User userOne = null;
		ResponseEntity<Object> responseEntity = null;
		userOne = userService.updateUser(user);
		if (userOne != null) {
			responseEntity = new ResponseEntity<Object>(userOne, HttpStatus.ACCEPTED);

		} else {
			responseEntity = new ResponseEntity<Object>("failure please check",
					HttpStatus.BAD_REQUEST);

		}
		return responseEntity;

	}

}
