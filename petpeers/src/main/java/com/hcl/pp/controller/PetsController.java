package com.hcl.pp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.pp.model.Pet;
import com.hcl.pp.model.ResponseMessage;
import com.hcl.pp.service.PetService;

@RestController
public class PetsController {


	@Autowired
	private PetService petService;

	@PostMapping(value = "/pets/addPet", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseMessage> addPet(@Valid @RequestBody Pet pet) {
		ResponseEntity<ResponseMessage> responseEntity = null;
		String statusOfAddPets = petService.savePet(pet);
		ResponseMessage responseMessage = new ResponseMessage();
		if (statusOfAddPets != null) {
			responseMessage.setMessage(statusOfAddPets);
			responseEntity = new ResponseEntity<ResponseMessage>(responseMessage, HttpStatus.CREATED);
		} else {
			responseMessage.setMessage("invalid status check Again");
			responseEntity = new ResponseEntity<ResponseMessage>(responseMessage, HttpStatus.BAD_REQUEST);

		}


		return responseEntity;
	}

	@GetMapping(value = "/pets", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Object>> petHome() {
		List<Pet> pets = null;
		List<Object> errorMessageObject = null;
		ResponseEntity<List<Object>> responseEntity = null;
		pets = petService.getAllPets();
		if (pets != null && pets.size() > 0) {
			List<Object> petObjects = new ArrayList<Object>();
			petObjects.addAll(pets);
			responseEntity = new ResponseEntity<List<Object>>(petObjects, HttpStatus.CREATED);
			return responseEntity;
		} else {
			errorMessageObject = new ArrayList<Object>();
			errorMessageObject.add("sorry no pets available");
			responseEntity = new ResponseEntity<List<Object>>(errorMessageObject, HttpStatus.BAD_REQUEST);

		}


		return responseEntity;

	}

	@GetMapping(value = "/pets/petDetail/{petId}")
	public ResponseEntity<Object> petDetail(@PathVariable long petId) {
		Pet pet = null;
		ResponseEntity<Object> responseEntity = null;
		pet = petService.getPetById(petId);
		if (pet != null) {
			responseEntity = new ResponseEntity<Object>(pet, HttpStatus.FOUND);

		} else {

			responseEntity = new ResponseEntity<Object>("update failed please check",
					HttpStatus.BAD_REQUEST);

		}

		return responseEntity;

	}

	@PutMapping(value = "/pets/updatePet", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> updatePetDetails(@Valid @RequestBody Pet pet) {
		Pet petOne = null;
		ResponseEntity<Object> responseEntity = null;
		petOne = petService.updatePet(pet);
		if (petOne != null) {
			responseEntity = new ResponseEntity<Object>(pet, HttpStatus.ACCEPTED);

		} else {
			responseEntity = new ResponseEntity<Object>("Upadate Failed Check Pet Object Details",
					HttpStatus.BAD_REQUEST);

		}
		return responseEntity;

	}

	@DeleteMapping(value = "/pets/deletePet/{petId}")
	public ResponseEntity<ResponseMessage> deletePetById(@PathVariable long petId) {
		ResponseEntity<ResponseMessage> responseEntity = null;
		ResponseMessage responseMessage = new ResponseMessage();
		String statausOfDeletePet = petService.deletePetById(petId);

		if (statausOfDeletePet != null) {
			responseMessage.setMessage(statausOfDeletePet);
			responseEntity = new ResponseEntity<ResponseMessage>(responseMessage, HttpStatus.GONE);
		} else {
			responseMessage.setMessage("invalid status");
			responseEntity = new ResponseEntity<ResponseMessage>(responseMessage, HttpStatus.BAD_REQUEST);

		}

		return responseEntity;

	}

}
