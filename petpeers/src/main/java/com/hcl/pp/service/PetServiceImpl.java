package com.hcl.pp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.pp.model.Pet;
import com.hcl.pp.repository.PetRepository;

@Service
public class PetServiceImpl implements PetService {

	@Autowired
	private PetRepository petRepository;

	@Override
	public String savePet(Pet pet) {

		String saveStatus = null;
		if (pet != null && pet.getName() != null && pet.getPlace() != null && pet.getAge() > 0) {

			Pet saveObj = petRepository.save(pet);
			if (saveObj!=null) {
				saveStatus="pet successfully added";
			}
			else {
				saveStatus="sorry we can't add the pet";
			}
		}

		return saveStatus;
	}

	@Override
	public List<Pet> getAllPets() {

		return petRepository.findAllRecord();
	}

	@Override
	public Pet getPetById(long petId) {
		Pet pet = null;
		Optional<Pet> optionalPet = petRepository.findById(petId);
		if (optionalPet.isPresent()) {
			pet = optionalPet.get();
		}

		return pet;
	}

	@Override
	public Pet updatePet(Pet pet) {
		Pet petTwo = null;
		Optional<Pet> optionalPet = petRepository.findById(pet.getId());
		if (optionalPet.isPresent()) {
			petTwo = petRepository.save(pet);
		}
		return petTwo;
	}

	@Override
	public String deletePetById(long petId) {
		String removalStatus = null;
		Optional<Pet> optionalPet = petRepository.findById(petId);

		if (optionalPet.isPresent()) {
			petRepository.deleteById(petId);
			removalStatus = "Pet Record Is Removed";
		} else {
			removalStatus = "pet is not present";

		}
		return removalStatus;
	}

}
