package com.hcl.udemySpringRest.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hcl.udemySpringRest.main.Groceries;
@Repository
public interface GroceryDao extends CrudRepository<Groceries, Long> {

}
