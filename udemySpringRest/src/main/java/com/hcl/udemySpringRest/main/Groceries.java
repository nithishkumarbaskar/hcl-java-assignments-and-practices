package com.hcl.udemySpringRest.main;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name = "groceries")
public class Groceries {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="gid")
	@NotNull
	private long gid;
	
	@Column(name="gname")
	@NotNull
	private String gname;
	
	@Column(name="marketName")
	@NotNull
	private String marketName;

	public long getGid() {
		return gid;
	}

	public void setGid(long l) {
		this.gid = l;
	}

	public String getGname() {
		return gname;
	}

	public void setGname(String gname) {
		this.gname = gname;
	}

	public String getMarketName() {
		return marketName;
	}

	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	public Groceries() {
		super();
	}

	public Groceries(int gid, String gname, String marketName) {
		super();
		this.gid = gid;
		this.gname = gname;
		this.marketName = marketName;
	}

}
