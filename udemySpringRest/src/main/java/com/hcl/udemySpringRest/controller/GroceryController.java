package com.hcl.udemySpringRest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.udemySpringRest.main.Groceries;
import com.hcl.udemySpringRest.service.GroceryService;

@RestController
@RequestMapping("shop")
public class GroceryController {
	@Autowired
	private GroceryService groceryService;

	// for adding groceries
	@PostMapping(value = "add")
	public ResponseEntity<Groceries> saveGrocery(@RequestBody Groceries groceries) {

		return new ResponseEntity<Groceries>(groceryService.saveGrocery(groceries), HttpStatus.CREATED);

	}

	// for fetching details of grocery
	@GetMapping(value = "groceries")
	public Iterable<Groceries> getAllGroceries() {
		return groceryService.getAllGrocery();
	}

	// for fetching particular grocery by id
	@GetMapping(value = "{id}")
	public Groceries getGroceryById(@PathVariable Long id) {
		return groceryService.getGroceryById(id);
	}

	// for updating grocery by id
	@PutMapping(value = "{id}")
	public ResponseEntity<Groceries> updateGrocery(@PathVariable Long id, @RequestBody Groceries groceries) {
		return new ResponseEntity<Groceries>(groceryService.updateGroceryById(id, groceries), HttpStatus.OK);

	}

	@DeleteMapping(value = "{id}")
	public String deletebyId(@PathVariable Long id) {
		groceryService.deleteById(id);
		return "user deleted";
	}

}
