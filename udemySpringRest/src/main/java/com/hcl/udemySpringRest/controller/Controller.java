package com.hcl.udemySpringRest.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.udemySpringRest.main.Groceries;

@RestController
@RequestMapping("user")
public class Controller {
	@GetMapping(value = "getMethod")
	public String getShow() {
		return "get method is running";
	}

	//
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Groceries> getIdShow(@PathVariable int id) {
		Groceries groceries = new Groceries(id, "carrot", "chennai");
		return new ResponseEntity<Groceries>(groceries, HttpStatus.OK);

	}

	/*
	 * // in request parameter annotation defaultvalue,required can be used for
	 * optional parameters in uri
	 * 
	 * @GetMapping public String getIdReqShow(@RequestParam(value = "id") int
	 * id, @RequestParam(value = "name",required=false) String name) { return
	 * "Hi get method is running with id:" + id + "and user name is " + name; }
	 */
/*
	@PostMapping(value = "postMethod",  produces = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<Groceries> postIdShow( @RequestBody Groceries groceries1) {
		Groceries groceries = new Groceries(groceries1.getGid(), groceries1.getGname(), groceries1.getMarketName());
		ResponseEntity<Groceries> responseEntity= null;
		responseEntity=new ResponseEntity<Groceries>(groceries, HttpStatus.OK);
		return responseEntity;
	}*/

	@PutMapping(value = "putMethod")
	public String putShow() {
		return "Hi put method is running";
	}

	@DeleteMapping(value = "deleteMethod")
	public String deleteShow() {
		return "Hi delete method is running";
	}

}
