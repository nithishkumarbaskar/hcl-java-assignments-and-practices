package com.hcl.udemySpringRest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UdemySpringRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(UdemySpringRestApplication.class, args);
	}

}
