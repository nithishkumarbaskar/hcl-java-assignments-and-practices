package com.hcl.udemySpringRest.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.udemySpringRest.dao.GroceryDao;
import com.hcl.udemySpringRest.main.Groceries;

@Service
public class GroceryServiceImpl implements GroceryService {
	@Autowired
	private GroceryDao groceryDao;

	@Override
	public Groceries saveGrocery(Groceries groceries) {

		return groceryDao.save(groceries);
	}

	@Override
	public Iterable<Groceries> getAllGrocery() {
		return groceryDao.findAll();
	}

	@Override
	public Groceries getGroceryById(Long id) {
		Optional<Groceries> groceries = groceryDao.findById(id);
		if (groceries.isPresent()) {
			return groceries.get();
		} else {
			return null;
		}
	}

	@Override
	public Groceries updateGroceryById(Long id, Groceries groceries) {
		Groceries UpdatedGrocery = groceryDao.findById(id).orElseThrow(null);
		UpdatedGrocery.setGid(groceries.getGid());
		UpdatedGrocery.setGname(groceries.getGname());
		UpdatedGrocery.setMarketName(groceries.getMarketName());
		return UpdatedGrocery;
	}

	@Override
	public void deleteById(Long id) {
		groceryDao.deleteById(id);

	}

}
