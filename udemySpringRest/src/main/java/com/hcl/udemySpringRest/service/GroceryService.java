package com.hcl.udemySpringRest.service;


import com.hcl.udemySpringRest.main.Groceries;

public interface GroceryService {
	public abstract Groceries saveGrocery(Groceries groceries);
	public abstract Iterable<Groceries> getAllGrocery();
	public abstract Groceries getGroceryById(Long id);
	public abstract Groceries updateGroceryById(Long id,Groceries groceries);
	public abstract void deleteById(Long id);
}
