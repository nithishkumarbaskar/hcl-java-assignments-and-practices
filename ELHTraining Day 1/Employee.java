package assignment_1;
import java.util.Scanner;
public class Employee {

	public static void main(String[] args) {
		Scanner name=new Scanner(System.in);
	    System.out.println("Enter employee name: ");
	    String employeeName= name.nextLine();
		Scanner designation=new Scanner(System.in);
	    System.out.println("Enter employee designation: ");
		String employeeDesignation = designation.nextLine();
		Scanner age=new Scanner(System.in);
	    System.out.println("Enter employee age: ");
		int employeeAge = age.nextInt();
		Scanner mobile=new Scanner(System.in);
	    System.out.println("Enter employee mobile number: ");
		long employeeMobile = mobile.nextLong();

		MainDetails details = new MainDetails(employeeName, employeeDesignation, employeeAge, employeeMobile);
		for (int value = 1; value <= 5; value++) {
			System.out.println("count: " + value);
			System.out.println("Employee name: "+details.name);
			System.out.println("Employee designation: "+details.designation);
			System.out.println("Employee age: "+details.age);
			System.out.println("Employee mobile number: "+details.mobile);
		}

		// employeeDetails(employeeName,employeeDesignation,employeeAge,employeeMobile);

	}

}
