package assignment_1;

public class MainDetails {

	public static String name;
	public static String designation;
	public static int age;
	public static long mobile;

	MainDetails(String name, String designation, int age, long mobile) {
		this.name = name;
		this.designation = designation;
		this.age = age;
		this.mobile = mobile;
	}

	/*
	 * public static void employeeDetails(){ String name; String designation; int
	 * age; long mobile; System.out.println("employee name is "+name);
	 * System.out.println("employee Designation is "+designation);
	 * System.out.println("employee Age is "+age);
	 * System.out.println("employee mobile number is "+mobile); }
	 */
}
