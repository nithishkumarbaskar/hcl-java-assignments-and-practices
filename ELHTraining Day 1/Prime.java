package assignment_1;
import java.util.Scanner;
public class Prime {

	public static void main(String[] args) {
	    Scanner input=new Scanner(System.in);
	    System.out.println("Enter prime number:");
		int number = input.nextInt();
		int divisorCount = 0;
		for ( int count=1;count<=number;count++)
		{
		    if (number%count==0){
		       divisorCount++;
		    }
		}
		if (divisorCount==2){
		    System.out.println(number+" is a prime number");
		}
		else{
		    System.out.println(number+" is not a prime number");
		}

	}

}
