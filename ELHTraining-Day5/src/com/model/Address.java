/**
 * 
 */
package com.model;

/**
 * @author NITHISH KUMAR
 *
 */
public class Address {
	int doorNo;
	String streetName;
	String district;
	int pinCode;

	public Address(int doorNo, String streetName, String district, int pincode) {
		this.doorNo = doorNo;
		this.streetName = streetName;
		this.district = district;
		this.pinCode = pincode;
	}

	@Override
	public String toString() {
		return "The employee address is--" + doorNo + ", " + streetName + ", " + district + "-" + pinCode++;
	}

	public int getDoorNo() {
		return doorNo;
	}

	public void setDoorNo(int doorNo) {
		this.doorNo = doorNo;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public int getPinCode() {
		return pinCode;
	}

	public void setPinCode(int pinCode) {
		this.pinCode = pinCode;
	}
}
