/**
 * 
 */
package com.model;

/**
 * @author NITHISH KUMAR
 *
 */
public class Department {
	String employeeName;

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	String departmentName;
	int departmentId;

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public Department(String employeeName, String departmentName, int departmentId) {
		super();
		this.employeeName = employeeName;
		this.departmentName = departmentName;
		this.departmentId = departmentId;
	}

	@Override
	public String toString() {
		return "The employee " + employeeName + " belong to the Department of " + departmentName + ", departmentId is "
				+ departmentId++;
	}
}
