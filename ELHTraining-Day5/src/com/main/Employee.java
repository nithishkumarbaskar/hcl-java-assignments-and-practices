/**
 * 
 */
package com.main;

import com.model.Address;
import com.model.Department;

/**
 * @author NITHISH KUMAR
 *
 */
public class Employee {

	public static void main(String[] args) {
		Department department = new Department("nithish", "software engineer", 34);
		Address address = new Address(67, "emp street", "chennai", 600028);
		System.out.println(department);
		System.out.println(address);
	}

}
